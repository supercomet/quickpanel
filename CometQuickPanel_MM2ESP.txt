--[[

   _____                     _      ____        _      _    _____                 _ 
  / ____|                   | |    / __ \      (_)    | |  |  __ \               | |
 | |     ___  _ __ ___   ___| |_  | |  | |_   _ _  ___| | _| |__) |_ _ _ __   ___| |
 | |    / _ \| '_ ` _ \ / _ \ __| | |  | | | | | |/ __| |/ /  ___/ _` | '_ \ / _ \ |
 | |___| (_) | | | | | |  __/ |_  | |__| | |_| | | (__|   <| |  | (_| | | | |  __/ |
  \_____\___/|_| |_| |_|\___|\__|  \___\_\\__,_|_|\___|_|\_\_|   \__,_|_| |_|\___|_|   
  
  This Script is a Comet QuickPanel Script. This is based off a Random MM2 ESP.
]]-- 

game.StarterGui:SetCore("SendNotification",  {
	Title = "MM2 ESP";
	Text = "Press T to Refresh ESP, Press Z to Toggle ESP.";
	Duration = 10;
	Button1 = "OK";
})

local ESPToggle = true
local Mouse = game.Players.LocalPlayer:GetMouse()

local plrs = game:GetService("Players")
local faces = {"Back","Bottom","Front","Left","Right","Top"}
function MakeESP()
	for _, v in pairs(game.Players:GetChildren()) do if v.Name ~= game.Players.LocalPlayer.Name then
			local bgui = Instance.new("BillboardGui",v.Character.Head)
			bgui.Name = ("EGUI")
			bgui.AlwaysOnTop = true
			bgui.ExtentsOffset = Vector3.new(0,2,0)
			bgui.Size = UDim2.new(0,200,0,50)
			local nam = Instance.new("TextLabel",bgui)
			nam.Text = v.Name
			nam.BackgroundTransparency = 1
			nam.TextSize = 15
			nam.Font = ("GothamBold")
			nam.TextColor3 = Color3.new(255,255,255)
			nam.Size = UDim2.new(0,200,0,50)
			if v.Backpack:FindFirstChild("Gun") or v.Character:FindFirstChild("Gun") then
				for _, p in pairs(v.Character:GetChildren()) do
					if p.Name == ("Head") or p.Name == ("Torso") or p.Name == ("Right Arm") or p.Name == ("Right Leg") or p.Name == ("Left Arm") or p.Name == ("Left Leg") then 
						for _, f in pairs(faces) do
							local m = Instance.new("SurfaceGui",p)
							m.Name = ("EGUI")
							m.Face = f
							m.AlwaysOnTop = true
							local mf = Instance.new("Frame",m)
							mf.Size = UDim2.new(1,0,1,0)
							mf.BorderSizePixel = 0
							mf.BackgroundTransparency = 0.5
							mf.BackgroundColor3 = Color3.new(0,0,255)
						end
					end
				end
			elseif v.Backpack:FindFirstChild("Knife") or v.Character:FindFirstChild("Knife") then
				for _, p in pairs(v.Character:GetChildren()) do
					if p.Name == ("Head") or p.Name == ("Torso") or p.Name == ("Right Arm") or p.Name == ("Right Leg") or p.Name == ("Left Arm") or p.Name == ("Left Leg") then 
						for _, f in pairs(faces) do
							local m = Instance.new("SurfaceGui",p)
							m.Name = ("EGUI")
							m.Face = f
							m.AlwaysOnTop = true
							local mf = Instance.new("Frame",m)
							mf.Size = UDim2.new(1,0,1,0)
							mf.BorderSizePixel = 0
							mf.BackgroundTransparency = 0.5
							mf.BackgroundColor3 = Color3.new(255,0,0)
						end
					end
				end
			else
				for _, p in pairs(v.Character:GetChildren()) do
					if p.Name == ("Head") or p.Name == ("Torso") or p.Name == ("Right Arm") or p.Name == ("Right Leg") or p.Name == ("Left Arm") or p.Name == ("Left Leg") then 
						for _, f in pairs(faces) do
							local m = Instance.new("SurfaceGui",p)
							m.Name = ("EGUI")
							m.Face = f
							m.AlwaysOnTop = true
							local mf = Instance.new("Frame",m)
							mf.Size = UDim2.new(1,0,1,0)
							mf.BorderSizePixel = 0
							mf.BackgroundTransparency = 0.5
							mf.BackgroundColor3 = Color3.new(255,255,255)
						end
					end
				end
			end
		end
	end
end



function ClearESP()
	for _, v in pairs(game.Workspace:GetDescendants()) do
		if v.Name == ("EGUI") then
			v:Remove()
		end
	end
end

Mouse.KeyDown:Connect(function(k)
	if k == "z" then
		if ESPToggle == false then
			ESPToggle = true
			pcall(ClearESP)
			pcall(MakeESP);
			game.StarterGui:SetCore("SendNotification",  {
				Title = "MM2 ESP";
				Text = "ESP Enabled.";
				Duration = 2;
				Button1 = "OK";
			})
		else
			ESPToggle = false
			pcall(ClearESP)
			game.StarterGui:SetCore("SendNotification",  {
				Title = "MM2 ESP";
				Text = "ESP Disbaled.";
				Duration = 2;
				Button1 = "OK";
			})
		end
	end
end)

Mouse.KeyDown:Connect(function(k)
	if k == "t" then
		if ESPToggle == true then
			wait(1)
			pcall(ClearESP)
			pcall(MakeESP)
			game.StarterGui:SetCore("SendNotification",  {
				Title = "MM2 ESP";
				Text = "ESP Refreshed.";
				Duration = 2;
				Button1 = "OK";
			})
		end
	end
end)

game:GetService("Players").PlayerAdded:Connect(function(v)
	if ESPToggle == true then
		wait(1)
		pcall(ClearESP)
		pcall(MakeESP)
	end
end)

game:GetService("Players").PlayerRemoving:Connect(function(v)
	if ESPToggle == true then
		wait(1)
		pcall(ClearESP)
		pcall(MakeESP)
	end
end)

pcall(ClearESP)
pcall(MakeESP)

while wait(60) do
	if ESPToggle == true then
		wait(1)
		pcall(ClearESP)
		pcall(MakeESP)
	end
end

