--[[

   _____                     _      ____        _      _    _____                 _ 
  / ____|                   | |    / __ \      (_)    | |  |  __ \               | |
 | |     ___  _ __ ___   ___| |_  | |  | |_   _ _  ___| | _| |__) |_ _ _ __   ___| |
 | |    / _ \| '_ ` _ \ / _ \ __| | |  | | | | | |/ __| |/ /  ___/ _` | '_ \ / _ \ |
 | |___| (_) | | | | | |  __/ |_  | |__| | |_| | | (__|   <| |  | (_| | | | |  __/ |
  \_____\___/|_| |_| |_|\___|\__|  \___\_\\__,_|_|\___|_|\_\_|   \__,_|_| |_|\___|_|   
  
  This Script is a Comet QuickPanel Script.
]]-- 

game.StarterGui:SetCore("SendNotification",  {
	Title = "Hitbox Extender [P]";
	Text = "Press P to Toggle Hitbox Extender. [NOTE] Sometimes, Hitbox Extender may cause Collision to be problematic.";
	Duration = 10;
	Button1 = "OK";
})

local HitboxToggle = true
local Mouse = game.Players.LocalPlayer:GetMouse()

local plrs = game:GetService("Players")
local LP = game:GetService("Players").LocalPlayer
function ExtendHitbox()
	for _, v in pairs(game:GetService("Players"):GetPlayers()) do
		if v.Name ~= LP.Name and v.Character.UpperTorso.Color ~= LP.Character.UpperTorso.Color then
			v.Character.LowerTorso.CanCollide = false
			v.Character.LowerTorso.Material = "Neon"
			v.Character.LowerTorso.Transparency = 0.95
			v.Character.LowerTorso.Size = Vector3.new(15, 15, 15)
			v.Character.HumanoidRootPart.Size = Vector3.new(15, 15, 15)
		end
	end
end

function NormalHitbox()
	for _, v in pairs(game:GetService("Players"):GetPlayers()) do
		if v.Name ~= LP.Name and v.Character.UpperTorso.Color ~= LP.Character.UpperTorso.Color then
			v.Character.LowerTorso.CanCollide = false
			v.Character.LowerTorso.Material = "Neon"
			v.Character.LowerTorso.Transparency = 0.95
			v.Character.LowerTorso.Size = Vector3.new(2, 0.4, 1)
			v.Character.HumanoidRootPart.Size = Vector3.new(2, 2, 1)
		end
	end
end

Mouse.KeyDown:Connect(function(k)
	if k == "p" then
		if HitboxToggle == false then
			HitboxToggle = true
			pcall(ExtendHitbox)
			game.StarterGui:SetCore("SendNotification",  {
				Title = "Hitbox Extender [P]";
				Text = "Hitbox Expansion Enabled.";
				Duration = 2;
				Button1 = "OK";
			})
		else
			HitboxToggle = false
			wait(1)
			pcall(NormalHitbox)
			game.StarterGui:SetCore("SendNotification",  {
				Title = "Hitbox Extender [P]";
				Text = "Hitbox Expansion Disabled.";
				Duration = 2;
				Button1 = "OK";
			})
		end
	end
end)

pcall(ExtendHitbox)

while wait(1) do
	if HitboxToggle == true then
		pcall(ExtendHitbox)
	end
end

