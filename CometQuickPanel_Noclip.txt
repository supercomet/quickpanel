--[[

   _____                     _      ____        _      _    _____                 _ 
  / ____|                   | |    / __ \      (_)    | |  |  __ \               | |
 | |     ___  _ __ ___   ___| |_  | |  | |_   _ _  ___| | _| |__) |_ _ _ __   ___| |
 | |    / _ \| '_ ` _ \ / _ \ __| | |  | | | | | |/ __| |/ /  ___/ _` | '_ \ / _ \ |
 | |___| (_) | | | | | |  __/ |_  | |__| | |_| | | (__|   <| |  | (_| | | | |  __/ |
  \_____\___/|_| |_| |_|\___|\__|  \___\_\\__,_|_|\___|_|\_\_|   \__,_|_| |_|\___|_|   
  
  This Script is a Comet QuickPanel Script. This is based off Infinite Yield's Noclip.
]]-- 


game.StarterGui:SetCore("SendNotification",  {
	Title = "Noclip [C]";
	Text = "Press C to toggle Noclip.";
	Duration = 10;
	Button1 = "OK";
})

noclip = true
local Mouse = game.Players.LocalPlayer:GetMouse()

local Character = game.Players.LocalPlayer.Character
local RunService = game:GetService("RunService")
local Noclipping = nil

Mouse.KeyDown:Connect(function(k)
	if k == "c" then
		if noclip == false then
			noclip = true
			game.StarterGui:SetCore("SendNotification",  {
				Title = "Noclip [C]";
				Text = "Noclip Enabled.";
				Duration = 2;
				Button1 = "OK";
			})
		else
			noclip = false
			game.StarterGui:SetCore("SendNotification",  {
				Title = "Noclip [C]";
				Text = "Noclip Disabled.";
				Duration = 2;
				Button1 = "OK";
			})
		end
	end	
end)

wait(0.1)
local function NoclipLoop()
	if noclip == true and Character ~= nil then
		for _, child in pairs(Character:GetDescendants()) do
			if child:IsA("BasePart") and child.CanCollide == true then
				child.CanCollide = false
			end
		end
	end
end

RunService.Stepped:Connect(NoclipLoop)
