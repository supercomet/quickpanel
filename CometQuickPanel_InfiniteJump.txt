--[[

   _____                     _      ____        _      _    _____                 _ 
  / ____|                   | |    / __ \      (_)    | |  |  __ \               | |
 | |     ___  _ __ ___   ___| |_  | |  | |_   _ _  ___| | _| |__) |_ _ _ __   ___| |
 | |    / _ \| '_ ` _ \ / _ \ __| | |  | | | | | |/ __| |/ /  ___/ _` | '_ \ / _ \ |
 | |___| (_) | | | | | |  __/ |_  | |__| | |_| | | (__|   <| |  | (_| | | | |  __/ |
  \_____\___/|_| |_| |_|\___|\__|  \___\_\\__,_|_|\___|_|\_\_|   \__,_|_| |_|\___|_|   
  
  This Script is part of the Comet QuickPanel.
]]-- 

local Mouse = game.Players.LocalPlayer:GetMouse()
local InfiniteJump = false

Mouse.KeyDown:Connect(function(k)
	if k == "v" then
		if InfiniteJump == false then
			InfiniteJump = true
			game.StarterGui:SetCore("SendNotification",  {
			Title = "Infinite Jump [V]";
			Text = "Infinite Jump Enabled.";
			Duration = 2;
			Button1 = "OK";
			})
		else
			InfiniteJump = false
			game.StarterGui:SetCore("SendNotification",  {
			Title = "Infinite Jump [V]";
			Text = "Infinite Jump Disabled.";
			Duration = 2;
			Button1 = "OK";
			})
		end	
	end
end)

game:GetService("UserInputService").JumpRequest:connect(function()
	if InfiniteJump == true then
		game:GetService"Players".LocalPlayer.Character:FindFirstChildOfClass'Humanoid':ChangeState("Jumping")
	end
end)

game.StarterGui:SetCore("SendNotification",  {
	Title = "Infinite Jump [V]";
	Text = "Press V to toggle InfiniteJump.";
	Duration = 10;
	Button1 = "OK";
})
