--[[

   _____                     _      ____        _      _    _____                 _ 
  / ____|                   | |    / __ \      (_)    | |  |  __ \               | |
 | |     ___  _ __ ___   ___| |_  | |  | |_   _ _  ___| | _| |__) |_ _ _ __   ___| |
 | |    / _ \| '_ ` _ \ / _ \ __| | |  | | | | | |/ __| |/ /  ___/ _` | '_ \ / _ \ |
 | |___| (_) | | | | | |  __/ |_  | |__| | |_| | | (__|   <| |  | (_| | | | |  __/ |
  \_____\___/|_| |_| |_|\___|\__|  \___\_\\__,_|_|\___|_|\_\_|   \__,_|_| |_|\___|_|   
  
  This Script is a Comet QuickPanel Script. This is based off a random Chat Logger!
]]-- 

game.StarterGui:SetCore("SendNotification",  {
	Title = "QuickPanel ChatLogger";
	Text = "Press H to hide the QuickPanel. Press the Green Button to Stop Scanning, Press the Yellow Button to stop Auto-Scrolling";
	Duration = 10;
	Button1 = "OK";
})

local ChatLogger = Instance.new("ScreenGui")
local MainFrame = Instance.new("Frame")
local ScrollingFrame = Instance.new("ScrollingFrame")
local UIListLayout = Instance.new("UIListLayout")
local UICorner = Instance.new("UICorner")
local ToggleNotice = Instance.new("TextButton")
local UICorner_2 = Instance.new("UICorner")
local UICorner_3 = Instance.new("UICorner")
local ChatToggle = Instance.new("TextButton")
local ScrollToggle = Instance.new("TextButton")
local UICorner_4 = Instance.new("UICorner")
local UICorner_5 = Instance.new("UICorner")
local SearchBox = Instance.new("TextBox")
local UICorner_6 = Instance.new("UICorner")

ChatLogger.Name = "ChatLogger"
ChatLogger.Parent = gethui()
ChatLogger.ZIndexBehavior = Enum.ZIndexBehavior.Sibling

MainFrame.Name = "MainFrame"
MainFrame.Parent = ChatLogger
MainFrame.AnchorPoint = Vector2.new(0.5, 0.5)
MainFrame.BackgroundColor3 = Color3.fromRGB(30, 30, 30)
MainFrame.ClipsDescendants = true
MainFrame.Position = UDim2.new(0.5, 0, 0.5, 0)
MainFrame.Size = UDim2.new(0, 455, 0, 232)

ScrollingFrame.Parent = MainFrame
ScrollingFrame.Active = true
ScrollingFrame.BackgroundColor3 = Color3.fromRGB(40, 40, 40)
ScrollingFrame.BorderColor3 = Color3.fromRGB(30, 30, 30)
ScrollingFrame.BorderSizePixel = 2
ScrollingFrame.Position = UDim2.new(0, 0, 0.131185591, 0)
ScrollingFrame.Size = UDim2.new(1, 0, 0.868814409, 0)
ScrollingFrame.ScrollBarThickness = 6

UIListLayout.Parent = ScrollingFrame
UIListLayout.HorizontalAlignment = Enum.HorizontalAlignment.Right
UIListLayout.SortOrder = Enum.SortOrder.LayoutOrder

UICorner.CornerRadius = UDim.new(0, 3)
UICorner.Parent = ScrollingFrame

ToggleNotice.Name = "ToggleNotice"
ToggleNotice.Parent = MainFrame
ToggleNotice.BackgroundColor3 = Color3.fromRGB(255, 53, 53)
ToggleNotice.ClipsDescendants = true
ToggleNotice.Position = UDim2.new(0.00999999978, 0, 0.0179999992, 0)
ToggleNotice.Size = UDim2.new(0, 20, 0, 20)
ToggleNotice.Font = Enum.Font.GothamBold
ToggleNotice.Text = "H"
ToggleNotice.TextColor3 = Color3.fromRGB(255, 255, 255)
ToggleNotice.TextSize = 14.000

UICorner_2.CornerRadius = UDim.new(0, 3)
UICorner_2.Parent = ToggleNotice

UICorner_3.CornerRadius = UDim.new(0, 3)
UICorner_3.Parent = MainFrame

ChatToggle.Name = "ChatToggle"
ChatToggle.Parent = MainFrame
ChatToggle.BackgroundColor3 = Color3.fromRGB(123, 255, 87)
ChatToggle.ClipsDescendants = true
ChatToggle.Position = UDim2.new(0.061999999, 0, 0.0179999992, 0)
ChatToggle.Size = UDim2.new(0, 20, 0, 20)
ChatToggle.Font = Enum.Font.SourceSans
ChatToggle.Text = ""
ChatToggle.TextColor3 = Color3.fromRGB(0, 0, 0)
ChatToggle.TextSize = 14.000

ScrollToggle.Name = "ScrollToggle"
ScrollToggle.Parent = MainFrame
ScrollToggle.BackgroundColor3 = Color3.fromRGB(255, 241, 85)
ScrollToggle.ClipsDescendants = true
ScrollToggle.Position = UDim2.new(0.115000002, 0, 0.0179999992, 0)
ScrollToggle.Size = UDim2.new(0, 20, 0, 20)
ScrollToggle.Font = Enum.Font.SourceSans
ScrollToggle.Text = ""
ScrollToggle.TextColor3 = Color3.fromRGB(0, 0, 0)
ScrollToggle.TextSize = 14.000

UICorner_4.CornerRadius = UDim.new(0, 3)
UICorner_4.Parent = ChatToggle

UICorner_5.CornerRadius = UDim.new(0, 3)
UICorner_5.Parent = ScrollToggle

SearchBox.Parent = MainFrame
SearchBox.BackgroundColor3 = Color3.fromRGB(40, 40, 40)
SearchBox.BorderColor3 = Color3.fromRGB(30, 30, 30)
SearchBox.BorderSizePixel = 0
SearchBox.Position = UDim2.new(0.166999966, 0, 0.0170000009, 0)
SearchBox.Size = UDim2.new(0, 108, 0, 20)
SearchBox.Font = Enum.Font.Gotham
SearchBox.PlaceholderColor3 = Color3.fromRGB(197, 197, 197)
SearchBox.PlaceholderText = "Search Here"
SearchBox.Text = ""
SearchBox.TextColor3 = Color3.fromRGB(255, 255, 255)
SearchBox.TextSize = 12.000
SearchBox.TextStrokeColor3 = Color3.fromRGB(255, 255, 255)
SearchBox.TextWrapped = true

UICorner_6.CornerRadius = UDim.new(0, 3)
UICorner_6.Parent = SearchBox


local chatuitoggle = true
local Mouse = game.Players.LocalPlayer:GetMouse()

Mouse.KeyDown:Connect(function(k)
	if k == "h" then
		if chatuitoggle == false then
			chatuitoggle = true
			MainFrame.Visible = true
		else
			chatuitoggle = false
			MainFrame.Visible = false
		end
	end
end)

local logging = true
local autoscroll = true

ChatToggle.MouseButton1Down:Connect(function()
	logging = not logging
	if logging then ChatToggle.BackgroundColor3 = Color3.fromRGB(123, 255, 87) else ChatToggle.BackgroundColor3 = Color3.fromRGB(255, 125, 125) end
end)

ScrollToggle.MouseButton1Down:Connect(function()
	autoscroll = not autoscroll
	if autoscroll then ScrollToggle.BackgroundColor3 = Color3.fromRGB(255, 241, 85) else ScrollToggle.BackgroundColor3 = Color3.fromRGB(255, 193, 121) end
end)


function updateList()
	ScrollingFrame.CanvasSize = UDim2.new(offsetX, offsetY, scaleX, UIListLayout.AbsoluteContentSize.Y)
end

local prevOutputPos = 0
function output(plr, msg)
	if not logging then return end
	local msgtags = plr.Name
	local textcolors = Color3.fromRGB(255,255,255)

	if string.sub(msg, 1,1) == ":" or string.sub(msg,1,1) == ";" then msgtags = "(CMDS) "+plr.Name textcolors = Color3.fromRGB(255,0,0) elseif string.sub(msg,1,2) == "/w" or string.sub(msg,1,7) == "/whisper" then msgtags = "(PM) "+plr.Name textcolors = Color3.fromRGB(0,255,0) elseif string.sub(msg,1,5) == "/team" or string.sub(msg,1,2) == "/t" then msgtags = "(TEAM) "+plr.Name textcolors = Color3.fromRGB(0,0,255) else msgtags = plr.Name end

	local ChatLabel = Instance.new("TextLabel")

	ChatLabel.Name = "ChatLabel"
	ChatLabel.Text = msgtags .. ": " .. msg
	ChatLabel.Parent = ScrollingFrame
	ChatLabel.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
	ChatLabel.BackgroundTransparency = 1.000
	ChatLabel.BorderSizePixel = 0
	ChatLabel.ClipsDescendants = true
	ChatLabel.Position = UDim2.new(0.0395604409, 0, 0.00992236007, 0)
	ChatLabel.Size = UDim2.new(0.99000001, 0, 0, 20)
	ChatLabel.Font = Enum.Font.GothamBold
	ChatLabel.TextColor3 = Color3.fromRGB(255, 255, 255)
	ChatLabel.TextSize = 12.000
	ChatLabel.TextXAlignment = Enum.TextXAlignment.Left
	ChatLabel.TextColor3 = textcolors
	ChatLabel.TextWrapped = true
end

for i,v in pairs(game.Players:GetChildren()) do
	v.Chatted:Connect(function(msg)
		output(v, msg)
		wait(0.1)
		updateList()
	end)
end

game.Players.ChildAdded:Connect(function(plr)
	if plr:IsA("Player") then
		plr.Chatted:Connect(function(msg)
			output(plr, msg)
			wait(0.1)
			updateList()
		end)
	end
end)

local UserInputService = game:GetService("UserInputService")

local gui = MainFrame

local dragging
local dragInput
local dragStart
local startPos

local function update(input)
	local delta = input.Position - dragStart
	gui.Position = UDim2.new(startPos.X.Scale, startPos.X.Offset + delta.X, startPos.Y.Scale, startPos.Y.Offset + delta.Y)
end

gui.InputBegan:Connect(function(input)
	if input.UserInputType == Enum.UserInputType.MouseButton1 or input.UserInputType == Enum.UserInputType.Touch then
		dragging = true
		dragStart = input.Position
		startPos = gui.Position

		input.Changed:Connect(function()
			if input.UserInputState == Enum.UserInputState.End then
				dragging = false
			end
		end)
	end
end)

gui.InputChanged:Connect(function(input)
	if input.UserInputType == Enum.UserInputType.MouseMovement or input.UserInputType == Enum.UserInputType.Touch then
		dragInput = input
	end
end)

UserInputService.InputChanged:Connect(function(input)
	if input == dragInput and dragging then
		update(input)
	end
end)

local function UpdateCanvasPosition()
	if autoscroll == true then
		wait(0.1)
		ScrollingFrame.CanvasPosition = Vector2.new(0, UIListLayout.AbsoluteContentSize.Y)
	end
end 

UIListLayout:GetPropertyChangedSignal("AbsoluteContentSize"):Connect(UpdateCanvasPosition)

function UpdateResults()
	local search = string.lower(SearchBox.Text)
	for i, v in	 pairs(ScrollingFrame:GetChildren()) do
		if v:IsA("TextLabel") then
			if search ~= "" then
				logging = false
				local item = string.lower(v.Text)
				if string.find(item, search) then
					v.Visible = true
				else
					v.Visible = false
				end
			else
				v.Visible = true
				logging = true
			end
		end
	end
end

SearchBox.Changed:Connect(UpdateResults)
